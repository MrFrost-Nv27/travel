<?php
require 'vendor/autoload.php';
require 'helpers/database.php';
require 'helpers/basic.php';

use Dotenv\Dotenv;

$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->safeLoad();
$dotenv->required(['DB_HOST', 'DB_NAME', 'DB_USER', 'DB_PASS']);

$koneksi = new Koneksi(env("DB_HOST"), env("DB_USER"), env("DB_PASS"), env("DB_NAME"));

d($koneksi->cek());
