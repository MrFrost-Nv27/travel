<?php
if (!function_exists('env')) {
    function env(String $var)
    {
        return $_ENV[$var] ?? null;
    }
}
