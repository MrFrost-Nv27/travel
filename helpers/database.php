<?php

class Koneksi
{
    public $conn;
    public function __construct(
        private String $hostname,
        private String $username,
        private String $password,
        private String $dbname,
    ) {
        $this->conn = new mysqli($hostname, $username, $password);
    }

    public function cek()
    {
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
        return "Connected successfully";
    }
}
